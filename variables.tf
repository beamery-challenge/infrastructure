variable "do_token" {
  type        = string
  description = "Digital ocean access token"
}

variable "company" {
  type        = string
  description = "Company name"
}

variable "environment" {
  type        = string
  description = "Environment type"
}

variable "region" {
  type        = string
  description = "DigitalOcean region"
}

variable "k8s_version" {
  type        = string
  description = "Kubernetes version"
}

variable "node_plan" {
  type        = string
  description = "DigitalOcean node plan"
}

variable "auto_scale" {
  type = bool
}

variable "min_nodes" {
  type = number
}

variable "max_nodes" {
  type = number
}

variable "lb_size" {
  type = string
}

variable "lb_algorithm" {
  type = string
}

variable "email" {
  description = "Email address for lets encrypt and cloudflare"
  type        = string
}

variable "gitlab_token" {
  type = string
}

variable "project_path" {
  type = string
}

variable "agent_name" {
  type = string
}

variable "token_name" {
  type    = string
  default = "kas-token"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
}

variable "gitlab_graphql_api_url" {
  type    = string
  default = "https://gitlab.com/api/graphql"
}

variable "agent_namespace" {
  type    = string
  default = "gitlab-agent"
}

variable "apps_namespace" {
  type    = string
  default = "gitops-apps"
}


variable "cloudflare_api_key" {
  type = string
}

variable "cloudflare_zone_id" {
  type = string
}

# variable "letsencrypt_email" {
#   type = string
# }


variable "cert_manager_chart_name" {}

variable "cert_manager_version" {}

variable "cert_manager_repo" {}

# variable "ip_address" {}

# Ingress variables

variable "nginx_ingress_namespace" {
  type = string
}

variable "nginx_ingress_chart_name" {}

variable "nginx_ingress_repo" {}

# do mysql variables
variable "do_mysql_version" {}
variable "do_mysql_size" {}
variable "do_node_count" {}
variable "do_mysql_user" {}

# nfs variables
variable "nfs_chart_name" {}
variable "nfs_chart_repo" {}
variable "nfs_storage_class" {}
variable "nfs_storage_size" {}
variable "nfs_persistence_enabled" {}

# wordpress variables
variable "wordpress_chart_name" {}
variable "wordpress_chart_repo" {}
variable "wordpress_chart_version" {}
variable "wordpress_user" {}
variable "wordpress_password" {}
variable "wordpress_cache_enabled" {}
variable "wordpress_allow_empty_password" {}
variable "wordpress_replica_count" {}
variable "wordpress_ingress_enabled" {}
variable "wordpress_ingress_classname" {}
variable "wordpress_tls_enabled" {}
variable "wordpress_ingress_affinity" {}
variable "wordpress_ingress_affinity_cookie_name" {}
variable "wordpress_persistence_enabled" {}
variable "wordpress_persistence_storageclass" {}
variable "wordpress_persistence_size" {}
variable "wordpress_persistence_access_mode" {}
variable "wordpress_cert_manager_issuer" {}
variable "wordpress_memcache_enabled" {}
