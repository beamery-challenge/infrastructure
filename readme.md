# Terraform scripts to deploy infrastructure in DigitalOcean

## Requirements given and solutions
1) Load balancing traffic between the two or more APP servers.
    - Solved by Kubernetes cluster, load balancer and Nginx ingress.
2) Caching to minimise load on front end APP servers.
    - Solved with Memcached running in Kubernetes and W3 Total Cache plugin 
3) Session handling to ensure user sessions are sticky to the server that first served them
    - Solved with ingress Nginx annotations
4) High availability of the database back-end.
    - Solved with SaaS DigitalOcean Database cluster
    - Although it's ok to set up highly available MySQL as a Kubernetes service, I like the SaaS database better since it's much easier to back it up.

## Pipeline
The Terraform CD pipeline is built up considering the recommendations by Gitlab. 
- On the feature branch, a pipeline is running only validation, but linting [TFLint](https://github.com/terraform-linters/tflint) could also be added to assure consistency of the code. Since I work alone, there's no point in adding it.
- Merge Request will run validation and also merge review job, which is essentially terraform plan
- On merge to the main branch will trigger terraform apply and deploy infrastructure to DigitalOcean account.

## How to run it under different Gitlab accounts

1) You need to have a DigitalOcean account and API Token
    - The best way to get the token is to use DigitalOcean CLI (doctl) and run ```doctl auth init```. You can install doctl via brew.
2) Cloudflare account with working zone 
3) Clone this repository and push it to your GitLab.
4) You need to set the following environment variables in Gitlab
   ```
    DO_TOKEN=<DigitalOcean api token>
    GITLAB_TOKEN=<Gitlab access token>
    CLOUDFLARE_ZONE_ID=<cloudflare zone id>
    CLOUDFLARE_API_KEY=<cloudflare api key>
    WORDPRESS_USER=<username for wordpress login>
    WORDPRESS_PASSWORD=<password for wordpress login>
   ```
5) All the other environment variables are in terraform.tfvars file and need to be changed.
6) Pushing the main branch will trigger the deployment. It takes approx 10-13 minutes to set everything up.

Notes:
  - This setup is not production ready. DigitalOcean Volumes supports only ReadWriteOnce storage access mode.
  - nfs-server-provisioner is used to create volume for WordPress pods, but it's not highly available.
  - nfs-server-provisioner helm chart is deprecated. Much better solution would be to use [rook-nfs](https://rook.io) or [Longorn](https://longhorn.io) for distributed block storage.
  - I didn't add any repo/pipeline for WordPress development since this wasn't requested from the challenge. Usually, I would keep this in a separate repository under the same Gitlab group and deploy changes via the Gitlab agent I added with Terraform.
