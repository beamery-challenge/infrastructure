provider "helm" {
  kubernetes {
    host  = module.doks.k8s_endpoint
    token = module.doks.k8s_token

    cluster_ca_certificate = base64decode(
      module.doks.k8s_ca
    )
  }
}

provider "kubernetes" {
  host  = module.doks.k8s_endpoint
  token = module.doks.k8s_token

  cluster_ca_certificate = base64decode(
    module.doks.k8s_ca
  )
}

