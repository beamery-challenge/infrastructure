
# General variables
company     = "beamery"
environment = "production"
region      = "ams3"
email       = "aimar@snapit.ee"

# Kubernetes variables
k8s_version  = "1.22.8-do.1"
node_plan    = "s-2vcpu-4gb"
auto_scale   = true
min_nodes    = 2
max_nodes    = 3
lb_size      = "lb-small"
lb_algorithm = "round_robin"

# Gitlab variables
agent_name   = "gitlab-agent"
project_path = "beamery-challenge/infrastructure"

# cert-manager variables
cert_manager_chart_name = "cert-manager"
cert_manager_version    = "v1.8.0"
cert_manager_repo       = "https://charts.jetstack.io"

# ingress variables
nginx_ingress_namespace  = "ingress-nginx"
nginx_ingress_chart_name = "nginx-ingress-controller"
nginx_ingress_repo       = "https://charts.bitnami.com/bitnami"

# DigitalOcean Mysql variables
do_mysql_version = "8"
do_mysql_size    = "db-s-1vcpu-2gb"
do_node_count    = 2
do_mysql_user    = "wordpress"

# NFS variables
nfs_chart_name          = "nfs-server-provisioner"
nfs_chart_repo          = "https://charts.helm.sh/stable"
nfs_persistence_enabled = "true"
nfs_storage_class       = "do-block-storage"
nfs_storage_size        = "20Gi"

# wordpress variables
wordpress_chart_name                   = "wordpress"
wordpress_chart_repo                   = "https://charts.bitnami.com/bitnami"
wordpress_chart_version                = "14.0.9"
wordpress_allow_empty_password         = "false"
wordpress_replica_count                = 2
wordpress_ingress_enabled              = "true"
wordpress_ingress_classname            = "nginx"
wordpress_tls_enabled                  = "true"
wordpress_cert_manager_issuer          = "letsencrypt-production"
wordpress_ingress_affinity             = "cookie"
wordpress_ingress_affinity_cookie_name = "stickysession"
wordpress_persistence_enabled          = "true"
wordpress_persistence_storageclass     = "nfs"
wordpress_persistence_size             = "10Gi"
wordpress_persistence_access_mode      = "ReadWriteMany"
wordpress_memcache_enabled             = "true"
wordpress_cache_enabled                = true