variable "nfs_chart_name" {}
variable "nfs_chart_repo" {}
variable "nfs_storage_class" {}
variable "nfs_storage_size" {}
variable "nfs_persistence_enabled" {}