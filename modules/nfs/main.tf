resource "helm_release" "nfs" {
  name       = var.nfs_chart_name
  repository = var.nfs_chart_repo
  chart      = var.nfs_chart_name

  set {
    name  = "persistence.enabled"
    value = var.nfs_persistence_enabled
  }

  set {
    name  = "persistence.storageClass"
    value = var.nfs_storage_class
  }

  set {
    name  = "persistence.size"
    value = var.nfs_storage_size
  }
}