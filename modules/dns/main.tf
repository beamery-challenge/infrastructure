terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "cloudflare" {
  email   = var.email
  api_key = var.cloudflare_api_key
}

resource "cloudflare_record" "domain" {
  zone_id = var.cloudflare_zone_id
  name    = var.subdomain
  value   = var.ip_address
  type    = "A"
  ttl     = 3600

  depends_on = [var.dns_depends_on]
}
