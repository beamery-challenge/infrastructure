variable "email" {}
variable "cloudflare_api_key" {}
variable "cloudflare_zone_id" {}
variable "subdomain" {}
variable "ip_address" {}
variable "dns_depends_on" {}