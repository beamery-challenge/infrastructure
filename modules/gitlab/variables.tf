variable "gitlab_token" {
  type = string
}

variable "project_path" {
  type = string
}

variable "agent_name" {
  type = string
}

variable "token_name" {
  type    = string
  default = "kas-token"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
}

variable "gitlab_graphql_api_url" {
  type    = string
  default = "https://gitlab.com/api/graphql"
}

variable "agent_namespace" {
  type    = string
  default = "gitlab-agent"
}

variable "apps_namespace" {
  type    = string
  default = "gitops-apps"
}

variable "k8s_endpoint" {}
variable "k8s_token" {}
variable "k8s_ca" {}