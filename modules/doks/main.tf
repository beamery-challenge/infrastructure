terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

resource "digitalocean_project" "workspace" {
  name        = var.workspace_name
  environment = var.environment
}

resource "digitalocean_kubernetes_cluster" "k8s" {
  name    = var.cluster_name
  region  = var.region
  version = var.k8s_version

  node_pool {
    name       = "${var.cluster_name}-pool"
    size       = var.node_plan
    auto_scale = var.auto_scale
    min_nodes  = var.min_nodes
    max_nodes  = var.max_nodes
  }
}

resource "digitalocean_loadbalancer" "ingress_load_balancer" {
  name      = "${var.cluster_name}-lb"
  region    = var.region
  size      = var.lb_size
  algorithm = var.lb_algorithm

  forwarding_rule {
    entry_port      = 80
    entry_protocol  = "http"
    target_port     = 80
    target_protocol = "http"
  }

  lifecycle {
    ignore_changes = [
      forwarding_rule,
    ]
  }
}

# resource "digitalocean_project_resources" "project" {
#   project = digitalocean_project.workspace.id
#   resources = [
#     digitalocean_kubernetes_cluster.k8s.urn,
#     digitalocean_loadbalancer.ingress_load_balancer.urn
#   ]
# }