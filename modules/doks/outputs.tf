output "k8s_endpoint" {
  value = digitalocean_kubernetes_cluster.k8s.endpoint
}

output "k8s_token" {
  value = digitalocean_kubernetes_cluster.k8s.kube_config[0].token
}

output "k8s_ca" {
  value = digitalocean_kubernetes_cluster.k8s.kube_config[0].cluster_ca_certificate
}

output "k8s_id" {
  value = digitalocean_kubernetes_cluster.k8s.id
}

output "loadbalancer_id" {
  value = digitalocean_loadbalancer.ingress_load_balancer.id
}

output "loadbalancer_ip" {
  value = digitalocean_loadbalancer.ingress_load_balancer.ip
}