variable "workspace_name" {
  type = string
}
variable "cluster_name" {
  type = string
}
variable "environment" {
  type = string
}
variable "region" {
  type = string
}
variable "k8s_version" {
  type = string
}
variable "node_plan" {
  type = string
}
variable "auto_scale" {
  type = bool
}
variable "min_nodes" {
  type = number
}
variable "max_nodes" {
  type = number
}

variable "lb_size" {
  type = string
}

variable "lb_algorithm" {
  type = string
}