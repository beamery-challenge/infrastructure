provider "helm" {
  kubernetes {
    host  = var.kubernetes.host
    token = var.kubernetes.token

    cluster_ca_certificate = var.kubernetes.cluster_ca_certificate
  }
}

resource "kubernetes_namespace" "ingress" {
  metadata {
    name = var.nginx_ingress_namespace
  }
}

resource "helm_release" "nginx_ingress_chart" {
  name       = var.nginx_ingress_chart_name
  namespace  = var.nginx_ingress_namespace
  repository = var.nginx_ingress_repo
  chart      = var.nginx_ingress_chart_name

  # set {
  #   name  = "service.type"
  #   value = "LoadBalancer"
  # }

  # set {
  #   name  = "service.annotations.kubernetes\\.digitalocean\\.com/load-balancer-id"
  #   value = var.lb_ip_address
  # }

  depends_on = [
    var.nginx_ingress_depends_on
  ]
}
