variable "nginx_ingress_namespace" {
  type = string
}

variable "nginx_ingress_chart_name" {}

variable "nginx_ingress_repo" {}

# variable "lb_ip_address" {}

variable "nginx_ingress_depends_on" {
  type = list
}

variable "kubernetes" {}
