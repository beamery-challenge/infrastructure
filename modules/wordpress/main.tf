resource "helm_release" "wordpress" {
  name          = var.wordpress_chart_name
  repository    = var.wordpress_chart_repo
  chart         = var.wordpress_chart_name
  version       = var.wordpress_chart_version
  wait          = true
  wait_for_jobs = true
  # timeout    = 600

  set {
    name  = "wordpressUsername"
    value = var.wordpress_user
  }

  set {
    name  = "wordpressPassword"
    value = var.wordpress_password
  }

  set {
    name  = "allowEmptyPassword"
    value = var.wordpress_allow_empty_password
  }

  set {
    name  = "wordpressConfigureCache"
    value = var.wordpress_cache_enabled
  }

  set {
    name  = "replicaCount"
    value = var.wordpress_replica_count
  }

  set {
    name  = "service.loadBalancerIP"
    value = var.loadbalancer_ip
  }

  set {
    name  = "service.annotations.kubernetes\\.digitalocean\\.com/load-balancer-id"
    value = var.loadbalancer_id
  }

  set {
    name  = "ingress.enabled"
    value = var.wordpress_ingress_enabled
  }

  set {
    name  = "ingress.ingressClassName"
    value = var.wordpress_ingress_classname
  }

  set {
    name  = "ingress.hostname"
    value = var.wordpress_hostname
  }

  set {
    name  = "ingress.tls"
    value = var.wordpress_tls_enabled
  }

  set {
    name  = "ingress.annotations.cert-manager\\.io/cluster-issuer"
    value = var.wordpress_cert_manager_issuer
  }

  set {
    name  = "ingress.annotations.nginx\\.ingress\\.kubernetes\\.io/affinity"
    value = var.wordpress_ingress_affinity
  }

  set {
    name  = "ingress.annotations.nginx\\.ingress\\.kubernetes\\.io/session-cookie-name"
    value = var.wordpress_ingress_affinity_cookie_name
  }

  set {
    name  = "persistence.enabled"
    value = var.wordpress_persistence_enabled
  }

  set {
    name  = "persistence.storageClass"
    value = var.wordpress_persistence_storageclass
  }

  set {
    name  = "persistence.size"
    value = var.wordpress_persistence_size
  }

  set {
    name  = "persistence.accessModes[0]"
    value = var.wordpress_persistence_access_mode
  }

  set {
    name  = "mariadb.enabled"
    value = "false"
  }

  set {
    name  = "externalDatabase.host"
    value = var.wordpress_database_hostname
  }

  set {
    name  = "externalDatabase.user"
    value = var.wordpress_database_user
  }

  set {
    name  = "externalDatabase.password"
    value = var.wordpress_database_password
  }

  set {
    name  = "externalDatabase.database"
    value = var.wordpress_database_name
  }

  set {
    name  = "externalDatabase.port"
    value = var.wordpress_database_port
  }

  set {
    name  = "memcached.enabled"
    value = var.wordpress_memcache_enabled
  }
}