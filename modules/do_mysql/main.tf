terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

resource "digitalocean_database_cluster" "mysql" {
  name       = var.do_mysql_name
  engine     = "mysql"
  version    = var.do_mysql_version
  size       = var.do_mysql_size
  region     = var.region
  node_count = var.do_node_count
}

resource "digitalocean_database_user" "mysql-user" {
  cluster_id = digitalocean_database_cluster.mysql.id
  name       = var.do_mysql_user
}

resource "digitalocean_database_firewall" "mysq-fw" {
  cluster_id = digitalocean_database_cluster.mysql.id

  rule {
    type  = "k8s"
    value = var.k8s_id
  }
}