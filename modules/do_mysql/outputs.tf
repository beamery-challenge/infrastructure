output "host" {
  value = digitalocean_database_cluster.mysql.host
}

output "user" {
  value = digitalocean_database_user.mysql-user.name
}

output "password" {
  value = digitalocean_database_user.mysql-user.password
}

output "database" {
  value = digitalocean_database_cluster.mysql.database
}

output "port" {
  value = digitalocean_database_cluster.mysql.port
}