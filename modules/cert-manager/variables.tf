variable "cert_manager_chart_name" {
  type = string
}

variable "cert_manager_repo" {
  type = string
}

variable "cert_manager_version" {
  type = string
}

variable "email" {
  type = string
}

variable "kubernetes" {}
variable "cert_manager_depends_on" {}