resource "kubernetes_namespace" "cert-manager" {
  metadata {
    name = var.cert_manager_chart_name
  }
}

resource "helm_release" "cert-manager" {
  name       = var.cert_manager_chart_name
  repository = var.cert_manager_repo
  chart      = var.cert_manager_chart_name
  version    = var.cert_manager_version
  namespace  = var.cert_manager_chart_name
  # timeout    = 120

  depends_on = [
    var.cert_manager_depends_on,
    kubernetes_namespace.cert-manager
  ]
  
  set {
    name  = "createCustomResource"
    value = "true"
  }
  
  set {
    name  = "installCRDs"
    value = "true"
  }
}

resource "helm_release" "cluster-issuer" {
  name      = "cluster-issuer"
  chart     = "./helm/cluster-issuer"
  namespace = "kube-system"

  depends_on = [
    var.cert_manager_depends_on,
    helm_release.cert-manager,
  ]

  set {
    name  = "letsencrypt_email"
    value = var.email
  }
}