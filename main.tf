provider "digitalocean" {
  token = var.do_token
}

module "doks" {
  source = "./modules/doks"

  workspace_name = var.company
  cluster_name   = "${var.company}-k8s"
  environment    = var.environment
  region         = var.region
  k8s_version    = var.k8s_version
  node_plan      = var.node_plan
  auto_scale     = var.auto_scale
  min_nodes      = var.min_nodes
  max_nodes      = var.max_nodes
  lb_size        = var.lb_size
  lb_algorithm   = var.lb_algorithm
}

module "gitlab" {
  source = "./modules/gitlab"

  gitlab_token = var.gitlab_token
  project_path = var.project_path
  agent_name   = var.agent_name
  k8s_endpoint = module.doks.k8s_endpoint
  k8s_token    = module.doks.k8s_token
  k8s_ca       = module.doks.k8s_ca
}

module "dns" {
  source = "./modules/dns"

  email              = var.email
  cloudflare_api_key = var.cloudflare_api_key
  cloudflare_zone_id = var.cloudflare_zone_id
  subdomain          = var.company
  ip_address         = module.doks.loadbalancer_ip
  dns_depends_on     = [module.doks.loadbalancer_id]
}

module "cert-manager" {
  source = "./modules/cert-manager"

  cert_manager_chart_name = var.cert_manager_chart_name
  cert_manager_version    = var.cert_manager_version
  cert_manager_repo       = var.cert_manager_repo
  email                   = var.email
  kubernetes              = local.kubernetes
  cert_manager_depends_on = [
    module.doks
  ]
}

# module "ingress" {
#   source = "./modules/ingress"

#   nginx_ingress_namespace  = var.nginx_ingress_namespace
#   nginx_ingress_chart_name = var.nginx_ingress_chart_name
#   nginx_ingress_repo       = var.nginx_ingress_repo
#   kubernetes               = local.kubernetes

#   nginx_ingress_depends_on = [
#     module.doks
#   ]
# }

module "do_mysql" {
  source = "./modules/do_mysql"

  region           = var.region
  do_mysql_name    = "${var.company}-mysql-cluster"
  do_mysql_version = var.do_mysql_version
  do_mysql_size    = var.do_mysql_size
  do_node_count    = var.do_node_count
  do_mysql_user    = var.do_mysql_user
  k8s_id           = module.doks.k8s_id
}

module "nfs" {
  source = "./modules/nfs"

  nfs_chart_name          = var.nfs_chart_name
  nfs_chart_repo          = var.nfs_chart_repo
  nfs_persistence_enabled = var.nfs_persistence_enabled
  nfs_storage_class       = var.nfs_storage_class
  nfs_storage_size        = var.nfs_storage_size
}

module "wordpress" {
  source = "./modules/wordpress"

  wordpress_chart_name                   = var.wordpress_chart_name
  wordpress_chart_repo                   = var.wordpress_chart_repo
  wordpress_chart_version                = var.wordpress_chart_version
  wordpress_allow_empty_password         = var.wordpress_allow_empty_password
  wordpress_replica_count                = var.wordpress_replica_count
  wordpress_ingress_enabled              = var.wordpress_ingress_enabled
  wordpress_ingress_classname            = var.wordpress_ingress_classname
  wordpress_tls_enabled                  = var.wordpress_tls_enabled
  wordpress_cert_manager_issuer          = var.wordpress_cert_manager_issuer
  wordpress_ingress_affinity             = var.wordpress_ingress_affinity
  wordpress_ingress_affinity_cookie_name = var.wordpress_ingress_affinity_cookie_name
  wordpress_persistence_enabled          = var.wordpress_persistence_enabled
  wordpress_persistence_storageclass     = var.wordpress_persistence_storageclass
  wordpress_persistence_size             = var.wordpress_persistence_size
  wordpress_persistence_access_mode      = var.wordpress_persistence_access_mode
  wordpress_memcache_enabled             = var.wordpress_memcache_enabled
  wordpress_user                         = var.wordpress_user
  wordpress_cache_enabled                = var.wordpress_cache_enabled
  wordpress_password                     = var.wordpress_password
  wordpress_database_hostname            = module.do_mysql.host
  wordpress_database_user                = module.do_mysql.user
  wordpress_database_password            = module.do_mysql.password
  wordpress_database_name                = module.do_mysql.database
  wordpress_database_port                = module.do_mysql.port
  wordpress_hostname                     = module.dns.hostname
  loadbalancer_ip                        = module.doks.loadbalancer_ip
  loadbalancer_id                        = module.doks.loadbalancer_id
}